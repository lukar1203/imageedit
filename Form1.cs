﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Drawing2D;
using System.Runtime.Serialization.Formatters.Binary;
using ImageEdit.Shapes;

namespace ImageEdit
{
    public partial class Form1 : Form
    {
        private String fileName;
        private Point start;
        private Point end;
        private List<Point> lines = new List<Point>();
        private List<GShape> shapes = new List<GShape>();
        private List<GShape> selectedShapes = new List<GShape>();
        private Pen pen;

        private enum Tool
        {
            Line,
            Rectangle,
            Circle,
            Triangle,
            Polygon,
            Bucket,
            Magnifier,
            Selection,
            Pointer
        };

        private Tool currentTool;
        private Color color;
        private Color activeToolColor;
        private float penSize;
        private CanvasSize canvasSize;
        private List<Point> polygon = new List<Point>();
        private System.Drawing.Imaging.ImageFormat fileFormat;
        private bool isDrawing;
        private float zoom;
        private int x;
        private int y;
        private Point fillPoint, insertPoint;
        private Selection selection;
        private GShape clickedShape;

        public Form1()
        {
            InitializeComponent();
            start = new Point();
            end = new Point();
            penSize = 5.0f;
            color = new Color();
            color = Color.Black;
            pen = new Pen(color, penSize);
            fileName = "Bez nazwy";
            currentTool = Tool.Line;
            activeToolColor = new Color();
            activeToolColor = Color.CadetBlue;
            tLine.BackColor = activeToolColor;
            canvasSize = new CanvasSize(canvas);
            x = canvas.Width;
            y = canvas.Height;
            isDrawing = false;
            zoom = 1.0f;
            fillPoint = new Point();
            insertPoint = new Point();
            selection = new Selection();
            toolStripStatusLabel1.Text = "Zoom: " + zoom * 100 + "%";
            toolStripStatusLabel2.Text = "";
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            e.Graphics.ScaleTransform(zoom, zoom);

            int k = 0;

            if (currentTool == Tool.Bucket && shapes.Count > 0)
            {
                int lowestDiameter = 5000;
                for (int i = 0; i < shapes.Count(); i++)
                {
                    if (shapes[i].Contains(new Point((fillPoint.X), (fillPoint.Y))))
                    {
                        if (shapes[i].Diameter < lowestDiameter)
                        {
                            lowestDiameter = shapes[i].Diameter;
                            k = i;
                        }
                    }
                }
            }

            bool filled = false;

            for (int i = 0; i < shapes.Count(); i++)
            {
                if (currentTool == Tool.Pointer)
                {
                    if (shapes[i].Contains(new Point((start.X), (start.Y))))
                    {
                        clickedShape = shapes[i];
                    }

                    //if(moving)
                    //    shapes[i].moveTo(insertPoint.X, insertPoint.Y, insertPoint.X - 100, insertPoint.Y - 100);
                }

                if (currentTool == Tool.Selection)
                {
                    if (selection.Contains(shapes[i]))
                    {
                        selectedShapes.Add(DeepCopy<GShape>(shapes[i]));
                    }
                }

                if (currentTool == Tool.Bucket)
                {
                    if (filled == false && shapes[k].Contains(new Point((fillPoint.X), (fillPoint.Y))))
                    {
                        shapes[k].Fill(color);
                        filled = true;
                    }
                }
                shapes[i].Paint(e.Graphics);
            }


            switch (currentTool)
            {
                case Tool.Line:
                    e.Graphics.DrawLine(pen, start, end);
                    break;
                case Tool.Rectangle:
                    e.Graphics.DrawRectangle(pen, Math.Min(start.X, end.X), Math.Min(start.Y, end.Y),
                                             Math.Abs(end.X - start.X), Math.Abs(end.Y - start.Y));
                    break;
                case Tool.Circle:
                    e.Graphics.DrawEllipse(pen, start.X, start.Y, end.X - start.X, end.X - start.X);
                    break;
                case Tool.Triangle:
                    Point[] points = new Point[3];
                    points[0] = start;
                    points[1].X = start.X;
                    points[1].Y = end.Y;
                    points[2] = end;
                    e.Graphics.DrawPolygon(pen, points);
                    break;
                case Tool.Polygon:
                    if (polygon.Count > 0)
                    {
                        e.Graphics.DrawLine(pen, new Point(polygon[polygon.Count - 1].X, polygon[polygon.Count - 1].Y),
                                            end);

                        for (int j = 0; j < polygon.Count - 1; j++)
                            e.Graphics.DrawLine(pen, polygon[j].X, polygon[j].Y, polygon[j + 1].X, polygon[j + 1].Y);
                    }
                    break;
                case Tool.Selection:
                    using (
                        Pen brush = new Pen(new HatchBrush(HatchStyle.BackwardDiagonal, Color.Black, Color.White), 5.0f)
                        )
                    {
                        e.Graphics.DrawRectangle(brush, Math.Min(start.X, end.X), Math.Min(start.Y, end.Y),
                                                 Math.Abs(end.X - start.X), Math.Abs(end.Y - start.Y));
                    }
                    break;
            }

            e.Graphics.ResetTransform();
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            toolStripStatusLabel2.Text = "" + e.Location;
            insertPoint = e.Location;

            if (isDrawing == true)
            {
                end = TranslatePoint(e);
                if (clickedShape != null)
                    clickedShape.MoveTo(end.X, end.Y);
                canvas.Invalidate();
            }
        }

        private Point TranslatePoint(MouseEventArgs e)
        {
            if (zoom > 1.0f)
            {
                end.X = (int)(e.X - (e.X * (1.0f - (1.0f / zoom))));
                end.Y = (int)(e.Y - (e.Y * (1.0f - (1.0f / zoom))));
            }
            else if (zoom < 1.0f)
            {
                end.X = (int)(e.X * (1.0f / zoom));
                end.Y = (int)(e.Y * (1.0f / zoom));
            }
            else
            {
                end.X = e.X;
                end.Y = e.Y;
            }

            return end;
        }

        private void save_Click(object sender, EventArgs e)
        {
            SaveFileAs();
            this.Text = this.Text + fileName;
        }

        private void SaveFileAs()
        {
            saveFileDialog1.Filter = "Jpeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif|PNG Image|*.png";
            saveFileDialog1.Title = "Zapisz plik";
            saveFileDialog1.FileName = fileName;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (Bitmap bmp = new Bitmap(canvas.Width, canvas.Height))
                {
                    fileName = saveFileDialog1.FileName;
                    canvas.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));
                    switch (saveFileDialog1.FilterIndex)
                    {
                        case 1:
                            bmp.Save(fileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                            fileFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
                            break;
                        case 2:
                            bmp.Save(fileName, System.Drawing.Imaging.ImageFormat.Bmp);
                            fileFormat = System.Drawing.Imaging.ImageFormat.Bmp;
                            break;
                        case 3:
                            bmp.Save(fileName, System.Drawing.Imaging.ImageFormat.Gif);
                            fileFormat = System.Drawing.Imaging.ImageFormat.Gif;
                            break;
                        case 4:
                            bmp.Save(fileName, System.Drawing.Imaging.ImageFormat.Png);
                            fileFormat = System.Drawing.Imaging.ImageFormat.Png;
                            break;
                    }
                }
                this.Text = "ImageEdit - " + fileName;
            }
        }

        private void SaveFile()
        {
            if (fileName.Equals("Bez nazwy"))
                SaveFileAs();
            else
            {
                using (Bitmap bmp = new Bitmap(canvas.Width, canvas.Height))
                {
                    canvas.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));
                    bmp.Save(fileName, fileFormat);
                }
            }
        }

        private void colorPicker_Click(object sender, EventArgs e)
        {
            if (penColorDialog.ShowDialog() == DialogResult.OK)
            {
                colorPicker.BackColor = penColorDialog.Color;
                color = penColorDialog.Color;
                pen = new Pen(color, penSize);
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label2.Text = "" + trackBar1.Value;
            penSize = (float)trackBar1.Value;
            pen = new Pen(color, penSize);
        }

        private void zapiszJakoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileAs();
        }

        private void tLine_Click(object sender, EventArgs e)
        {
            ClearTools();
            tLine.BackColor = activeToolColor;
            currentTool = Tool.Line;
        }

        private void tRectangle_Click(object sender, EventArgs e)
        {
            ClearTools();
            tRectangle.BackColor = activeToolColor;
            currentTool = Tool.Rectangle;
        }

        private void tCircle_Click(object sender, EventArgs e)
        {
            ClearTools();
            tCircle.BackColor = activeToolColor;
            currentTool = Tool.Circle;
        }

        private void tTriangle_Click(object sender, EventArgs e)
        {
            ClearTools();
            tTriangle.BackColor = activeToolColor;
            currentTool = Tool.Triangle;
        }

        private void tPolygon_Click(object sender, EventArgs e)
        {
            ClearTools();
            tPolygon.BackColor = activeToolColor;
            currentTool = Tool.Polygon;
        }

        private void ClearTools()
        {
            tLine.BackColor = splitContainer1.Panel1.BackColor;
            tRectangle.BackColor = splitContainer1.Panel1.BackColor;
            tCircle.BackColor = splitContainer1.Panel1.BackColor;
            tTriangle.BackColor = splitContainer1.Panel1.BackColor;
            tPolygon.BackColor = splitContainer1.Panel1.BackColor;
            tBucket.BackColor = splitContainer1.Panel1.BackColor;
            tMagnifier.BackColor = splitContainer1.Panel1.BackColor;
            tSelection.BackColor = splitContainer1.Panel1.BackColor;
            tPointer.BackColor = splitContainer1.Panel1.BackColor;

            start = new Point();
            end = new Point();
            selection = new Selection();
            selectedShapes.Clear();
            fillPoint = new Point();

            isDrawing = false;
            canvas.Invalidate();
        }

        private void tłoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            shapes.Clear();
            if (bgColorDialog.ShowDialog() == DialogResult.OK)
            {
                canvas.BackColor = bgColorDialog.Color;
            }
        }

        private void nowyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            zoom = 1.0f;
            shapes.Clear();
            polygon.Clear();
            canvas.Invalidate();
            ClearTools();
        }

        private void wymiaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (canvasSize.ShowDialog() == DialogResult.OK)
            {
                canvas.Size = new Size(canvasSize.CanvasWidth, canvasSize.CanvasHeight);
                x = canvas.Width;
                y = canvas.Height;
            }
        }

        private void zapiszToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFile();
        }

        private void canvas_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && currentTool == Tool.Magnifier)
            {
                zoom += 0.25f;
                canvas.Size = new Size((int)(x * zoom), (int)(y * zoom));
            }

            if (e.Button == MouseButtons.Middle && currentTool == Tool.Magnifier)
            {
                if (zoom > 0.25f)
                {
                    zoom -= 0.25f;
                    canvas.Size = new Size((int)(x * zoom), (int)(y * zoom));
                }
            }

            if (clickedShape != null)
            {
                clickedShape.MoveTo(TranslatePoint(e).X, TranslatePoint(e).Y);
                clickedShape = null;
            }
            toolStripStatusLabel1.Text = "Zoom: " + zoom * 100 + "%";
            //label1.Text = " " + zoom;

            if (e.Button == MouseButtons.Left)
            {
                if (currentTool == Tool.Polygon || currentTool == Tool.Bucket)
                    isDrawing = true;

                if (isDrawing == false)
                {
                    start = TranslatePoint(e);
                    isDrawing = true;
                }
                else
                {
                    end = TranslatePoint(e);

                    fillPoint = end;

                    isDrawing = false;


                    switch (currentTool)
                    {
                        case Tool.Line:
                            shapes.Add(new GLine(start, end, color, penSize));
                            break;
                        case Tool.Rectangle:
                            shapes.Add(new GRectangle(start, end, color, penSize));
                            break;
                        case Tool.Circle:
                            shapes.Add(new GCircle(start, end, color, penSize));
                            break;
                        case Tool.Triangle:
                            shapes.Add(new GTriangle(start, end, color, penSize));
                            break;
                        case Tool.Polygon:
                            if (polygon.Count > 2 && (end.X > (polygon[0].X - 10)) && (end.X < (polygon[0].X + 10)) &&
                                (end.Y > (polygon[0].Y - 10)) && (end.Y < (polygon[0].Y + 10)))
                            {
                                shapes.Add(new GPolygon(polygon.ToArray(), color, penSize));
                                polygon.Clear();
                                isDrawing = false;
                            }
                            else
                            {
                                polygon.Add(new Point(end.X, end.Y));
                                isDrawing = true;
                            }
                            break;
                        case Tool.Selection:
                            selection = new Selection(Math.Min(start.X, end.X), Math.Min(start.Y, end.Y),
                                                      Math.Abs(end.X - start.X), Math.Abs(end.Y - start.Y));
                            break;
                    }
                }
            }
            canvas.Invalidate();
        }

        private void tBucket_Click(object sender, EventArgs e)
        {
            ClearTools();
            tBucket.BackColor = activeToolColor;
            currentTool = Tool.Bucket;
        }

        private void tMagnifier_Click(object sender, EventArgs e)
        {
            ClearTools();
            tMagnifier.BackColor = activeToolColor;
            currentTool = Tool.Magnifier;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("Czy chcesz zapisać zmiany w pliku?", "Wyjście",
                                                  MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (result == DialogResult.No)
            {
                Dispose(true);
                Application.Exit();
            }
            else if (result == DialogResult.Yes)
            {
                SaveFile();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void tSelection_Click(object sender, EventArgs e)
        {
            ClearTools();
            tSelection.BackColor = activeToolColor;
            currentTool = Tool.Selection;
        }

        private void usuńToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (currentTool == Tool.Selection)
            {
                foreach (GShape item in selectedShapes)
                    shapes.Remove(item);

                selectedShapes.Clear();
                canvas.Invalidate();
            }
        }

        private void kopiujToolStripMenuItem1_Click(object sender, EventArgs e)
        {
        }

        public static T DeepCopy<T>(T item)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, item);
            stream.Seek(0, SeekOrigin.Begin);
            T result = (T)formatter.Deserialize(stream);
            stream.Close();
            return result;
        }

        private void wklejToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (currentTool == Tool.Selection)
            {
                foreach (GShape item in selectedShapes)
                {
                    item.MoveTo(insertPoint.X, insertPoint.Y, selection.X, selection.Y);
                    shapes.Add(item);
                    label2.Text = "" + shapes.Count;
                }

                selectedShapes.Clear();
                canvas.Invalidate();
            }
        }

        private void tPointer_MouseClick(object sender, MouseEventArgs e)
        {
            ClearTools();
            tPointer.BackColor = activeToolColor;
            currentTool = Tool.Pointer;
        }
    }
}