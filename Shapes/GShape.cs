﻿using System;
using System.Drawing;

namespace ImageEdit.Shapes
{
    [Serializable]
    abstract class GShape
    {
        protected int diameter;

        public int Diameter
        {
            get { return diameter; }
            set { diameter = value; }
        }

        protected Point start;

        public Point Start
        {
            get { return start; }
            set { start = value; }
        }
        protected Point end;

        public Point End
        {
            get { return end; }
            set { end = value; }
        }
        protected Color color;

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }
        protected float penSize;

        public float PenSize
        {
            get { return penSize; }
            set { penSize = value; }
        }

        public abstract void Paint(Graphics g);
        public abstract bool Contains(Point point);
        public abstract void Fill(Color color);
        public abstract void MoveTo(int pX, int pY, int sX, int sY);
        public abstract void MoveTo(int pX, int pY);
    }
}
