﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace ImageEdit.Shapes
{
    [Serializable]
    class GCircle : GFillableShape
    {
        public GCircle(Point start, Point end, Color color, float penSize)
        {
            this.color = color;
            this.penSize = penSize;
            this.start = start;
            this.end = end;
            diameter = end.X - start.X;
        }

        public override void Paint(Graphics g)
        {
            if (IsFilled == true)
            {
                using (SolidBrush brush = new SolidBrush(BgColor))
                {
                    g.FillEllipse(brush, start.X, start.Y, end.X - start.X, end.X - start.X);
                }
            }
            using (Pen tPen = new Pen(color, penSize))
                g.DrawEllipse(tPen, start.X, start.Y, end.X - start.X, end.X - start.X);

        }

        public override bool Contains(Point point)
        {
            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddEllipse(start.X, start.Y, end.X - start.X, end.X - start.X);
                path.GetBounds();
                return path.IsVisible(point);
            }
        }

        public override void MoveTo(int pX, int pY, int sX, int sY)
        {
            int dx = start.X - sX;
            int dy = start.Y - sY;
            int x = end.X - start.X;
            int y = end.Y - start.Y;
            start.X = pX + dx;
            start.Y = pY + dy;
            end.X = start.X + x;
            end.Y = start.Y + y;
        }

        public override void MoveTo(int pX, int pY)
        {
            int x = end.X - start.X;
            int y = end.Y - start.Y;
            start.X = pX;
            start.Y = pY;
            end.X = start.X + x;
            end.Y = start.Y + y;
        }
    }
}
