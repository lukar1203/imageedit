﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace ImageEdit.Shapes
{
    [Serializable]
    class GLine : GShape
    {
        public GLine(Point start, Point end, Color color, float penSize)
        {
            this.color = color;
            this.penSize = penSize;
            this.start = start;
            this.end = end;
        }

        public override void Paint(Graphics g)
        {
            using (Pen tPen = new Pen(color, penSize))
                g.DrawLine(tPen, start, end);
        }

        public override bool Contains(Point point)
        {
            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddLine(start, end);
                RectangleF r = path.GetBounds();
                return r.Contains(point);
            }
        }

        public override void Fill(Color color) { }


        public override void MoveTo(int pX, int pY, int sX, int sY)
        {
            int dx = start.X - sX;
            int dy = start.Y - sY;
            int x = end.X - start.X;
            int y = end.Y - start.Y;
            start.X = pX + dx;
            start.Y = pY + dy;
            end.X = start.X + x;
            end.Y = start.Y + y;
        }

        public override void MoveTo(int pX, int pY)
        {
            int x = end.X - start.X;
            int y = end.Y - start.Y;
            start.X = pX;
            start.Y = pY;
            end.X = start.X + x;
            end.Y = start.Y + y;
        }
    }
}
