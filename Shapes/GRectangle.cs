﻿using System;
using System.Drawing;

namespace ImageEdit.Shapes
{
    [Serializable]
    class GRectangle : GFillableShape
    {
        public GRectangle(Point start, Point end, Color color, float penSize)
        {
            this.color = color;
            this.penSize = penSize;
            this.start = start;
            this.end = end;
            diameter = end.X - start.X;
            if ((end.Y - start.Y) > diameter)
                diameter = end.Y - start.Y;
        }



        public override void Paint(Graphics g)
        {
            if (IsFilled == true)
            {
                using (SolidBrush brush = new SolidBrush(BgColor))
                {
                    g.FillRectangle(brush, Math.Min(start.X, end.X), Math.Min(start.Y, end.Y), Math.Abs(end.X - start.X), Math.Abs(end.Y - start.Y));
                }
            }
            using (Pen tPen = new Pen(color, penSize))
                g.DrawRectangle(tPen, Math.Min(start.X, end.X), Math.Min(start.Y, end.Y), Math.Abs(end.X - start.X), Math.Abs(end.Y - start.Y));
        }

        public override bool Contains(Point point)
        {
            Rectangle r = new Rectangle(Math.Min(start.X, end.X), Math.Min(start.Y, end.Y), Math.Abs(end.X - start.X), Math.Abs(end.Y - start.Y));
            return r.Contains(point);
        }

        public override void MoveTo(int pX, int pY, int sX, int sY)
        {
            int dx = start.X - sX;
            int dy = start.Y - sY;
            int x = Math.Abs(end.X - start.X);
            int y = Math.Abs(end.Y - start.Y);
            start.X = pX + dx;
            start.Y = pY + dy;
            end.X = start.X + x;
            end.Y = start.Y + y;
        }

        public override void MoveTo(int pX, int pY)
        {
            int x = Math.Abs(end.X - start.X);
            int y = Math.Abs(end.Y - start.Y);
            start.X = pX;
            start.Y = pY;
            end.X = start.X + x;
            end.Y = start.Y + y;
        }
    }
}
