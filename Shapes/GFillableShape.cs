﻿using System;
using System.Drawing;

namespace ImageEdit.Shapes
{
    [Serializable]
    abstract class GFillableShape : GShape
    {
        protected Color bgColor;

        public Color BgColor
        {
            get { return bgColor; }
            set { bgColor = value; }
        }
        private bool isFilled;

        public bool IsFilled
        {
            get { return isFilled; }
            set { isFilled = value; }
        }

        public override void Fill(Color color)
        {
            this.bgColor = color;
            isFilled = true;
        }

    }
}
