﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace ImageEdit.Shapes
{
    [Serializable]
    class GPolygon : GFillableShape
    {
        private Point[] points;

        public Point[] Points
        {
            get { return points; }
            set { points = value; }
        }

        public GPolygon(Point[] points, Color color, float penSize)
        {
            this.color = color;
            this.penSize = penSize;
            this.points = points;
            diameter = CalculateDiameter();
        }

        public override void Paint(Graphics g)
        {
            if (IsFilled == true){
                using (SolidBrush brush = new SolidBrush(BgColor))
                {
                    g.FillPolygon(brush, points);
                }
            }
            using (Pen tPen = new Pen(color, penSize))
                g.DrawPolygon(tPen, points);
        }

        public override bool Contains(Point point)
        {
            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddPolygon(points);
                return path.IsVisible(point);
            }
        }

        public int CalculateDiameter()
        {
            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddPolygon(points);
                RectangleF rect = path.GetBounds();
                return (int) (rect.Width >= rect.Height ? rect.Width : rect.Height);
            }
        }

        public override void MoveTo(int pX, int pY, int sX, int sY)
        {
            int x, y;

            int dx = points[0].X - sX;
            int dy = points[0].Y - sY;


            for (int i = 1; i < points.Length; i++)
            {
                x = points[i].X - points[0].X;
                y = points[i].Y - points[0].Y;
                points[i].X = pX + x + dx;
                points[i].Y = pY + y + dy;
            }

            points[0].X = pX + dx;
            points[0].Y = pY + dy;
        }

        public override void MoveTo(int pX, int pY)
        {
            int x, y;
            for (int i = 1; i < points.Length; i++)
            {
                x = points[i].X - points[0].X;
                y = points[i].Y - points[0].Y;
                points[i].X = pX + x;
                points[i].Y = pY + y;
            }

            points[0].X = pX;
            points[0].Y = pY;
        }
    }
}
