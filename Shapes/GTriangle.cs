﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace ImageEdit.Shapes
{
    [Serializable]
    class GTriangle : GFillableShape
    {
        private Point[] points;

        public Point[] Points
        {
            get { return points; }
            set { points = value; }
        }

        public GTriangle(Point start, Point end, Color color, float penSize)
        {
            this.color = color;
            this.penSize = penSize;
            this.start = start;
            this.end = end;
            diameter = end.X - start.X;
            if ((end.Y - start.Y) > diameter)
                diameter = end.Y - start.Y;
        }

        public override void Paint(Graphics g)
        {
            points = new Point[3];
            points[0] = start;
            points[1].X = start.X;
            points[1].Y = end.Y;
            points[2] = end;
            if (IsFilled == true)
            {
                using (SolidBrush brush = new SolidBrush(BgColor))
                {
                    g.FillPolygon(brush, points);
                }
            }
            using (Pen tPen = new Pen(color, penSize))
                g.DrawPolygon(tPen, points);
        }

        public override bool Contains(Point point)
        {
            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddPolygon(points);
                return path.IsVisible(point);
            }
        }

        public override void MoveTo(int pX, int pY, int sX, int sY)
        {
            int dx = start.X - sX;
            int dy = start.Y - sY;
            int x = end.X - start.X;
            int y = end.Y - start.Y;
            start.X = pX + dx;
            start.Y = pY + dy;
            end.X = start.X + x;
            end.Y = start.Y + y;
        }

        public override void MoveTo(int pX, int pY)
        {
            int x = end.X - start.X;
            int y = end.Y - start.Y;
            start.X = pX;
            start.Y = pY;
            end.X = start.X + x;
            end.Y = start.Y + y;
        }
    }
}
