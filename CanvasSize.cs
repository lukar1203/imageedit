﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ImageEdit
{
    public partial class CanvasSize : Form
    {
        private BufferedPanel panel;
        private int canvasHeight;

        public int CanvasHeight
        {
            get { return canvasHeight; }
        }
        private int canvasWidth;

        public int CanvasWidth
        {
            get { return canvasWidth; }
        }

        public CanvasSize(BufferedPanel panel)
        {
            this.panel = panel;
            InitializeComponent();
            Size size = panel.Size;
            numericUpDown1.Value = size.Height;
            numericUpDown2.Value = size.Width;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void changeSizeButton_Click(object sender, EventArgs e)
        {
            canvasHeight = (int)numericUpDown1.Value;
            canvasWidth = (int)numericUpDown2.Value;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }
    }
}
