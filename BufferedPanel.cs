﻿namespace ImageEdit
{
    public class BufferedPanel : System.Windows.Forms.Panel
    {

        public BufferedPanel()
        {
            this.DoubleBuffered = true;
        }
    }
}
