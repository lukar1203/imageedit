﻿using System;
using System.Drawing.Drawing2D;
using System.Drawing;
using ImageEdit.Shapes;

namespace ImageEdit
{
    class Selection : IDisposable
    {
        private int x, y, width, height;

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }
        private GraphicsPath selection;
        private bool isActive;

        public Selection(int x, int y, int width, int height)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            selection = new GraphicsPath();
            selection.AddRectangle(new Rectangle(x, y, width, height));
            isActive = true;
        }

        public Selection()
        {
            isActive = false;
        }

        public bool Contains(GShape shape)
        {
            if (isActive)
            {
                bool result = false;

                if (shape is GLine)
                {
                    GLine line = (GLine)shape;
                    if (selection.IsVisible(line.Start) && selection.IsVisible(line.End))
                        return true;
                }
                if (shape is GRectangle)
                {
                    GRectangle rectangle = (GRectangle)shape;
                    if (selection.IsVisible(rectangle.Start) && selection.IsVisible(rectangle.End))
                        return true;
                }
                if (shape is GCircle)
                {
                    GCircle circle = (GCircle)shape;
                    if (selection.IsVisible(circle.Start) && selection.IsVisible(circle.End))
                        return true;
                }
                if (shape is GTriangle)
                {
                    GTriangle triangle = (GTriangle)shape;
                    for (int i = 0; i < triangle.Points.Length; i++)
                    {
                        if (selection.IsVisible(triangle.Points[i]))
                            result = true;
                        else
                            result = false;
                    }

                    return result;

                }
                if (shape is GPolygon)
                {
                    GPolygon polygon = (GPolygon)shape;

                    for (int i = 0; i < polygon.Points.Length; i++)
                    {
                        if (selection.IsVisible(polygon.Points[i]))
                            result = true;
                        else
                            result = false;
                    }

                    return result;
                }
            }
            return false;
        }

        public void Dispose()
        {
            if (selection != null)
                selection.Dispose();
        }
    }
}
