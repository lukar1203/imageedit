﻿namespace ImageEdit
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }

                selection.Dispose();
                pen.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.lineThickness = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tools = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.colorPicker = new System.Windows.Forms.Label();
            this.canvas = new ImageEdit.BufferedPanel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.kopiujToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.wklejToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.usuńToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nowyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.zapiszToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zapiszJakoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.zakończToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edycjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kopiujToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wklejToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuńToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.obrazToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wymiaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tłoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penColorDialog = new System.Windows.Forms.ColorDialog();
            this.bgColorDialog = new System.Windows.Forms.ColorDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tMagnifier = new System.Windows.Forms.PictureBox();
            this.tBucket = new System.Windows.Forms.PictureBox();
            this.tPolygon = new System.Windows.Forms.PictureBox();
            this.tTriangle = new System.Windows.Forms.PictureBox();
            this.tCircle = new System.Windows.Forms.PictureBox();
            this.tRectangle = new System.Windows.Forms.PictureBox();
            this.tLine = new System.Windows.Forms.PictureBox();
            this.tPointer = new System.Windows.Forms.PictureBox();
            this.tSelection = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tMagnifier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tBucket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tPolygon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTriangle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tCircle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tRectangle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tPointer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tSelection)).BeginInit();
            this.SuspendLayout();
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "d";
            // 
            // lineThickness
            // 
            this.lineThickness.Location = new System.Drawing.Point(-1, 333);
            this.lineThickness.Name = "lineThickness";
            this.lineThickness.Size = new System.Drawing.Size(119, 13);
            this.lineThickness.TabIndex = 4;
            this.lineThickness.Text = "Grubość lini";
            this.lineThickness.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tools);
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Panel1.Controls.Add(this.trackBar1);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.colorPicker);
            this.splitContainer1.Panel1.Controls.Add(this.lineThickness);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.canvas);
            this.splitContainer1.Size = new System.Drawing.Size(984, 637);
            this.splitContainer1.SplitterDistance = 120;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 5;
            // 
            // tools
            // 
            this.tools.Location = new System.Drawing.Point(1, 4);
            this.tools.Name = "tools";
            this.tools.Size = new System.Drawing.Size(120, 13);
            this.tools.TabIndex = 8;
            this.tools.Text = "Narzędzia";
            this.tools.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tMagnifier, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tBucket, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tPolygon, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tTriangle, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tCircle, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tRectangle, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tLine, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tPointer, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tSelection, 0, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 20);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(120, 300);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(11, 349);
            this.trackBar1.Minimum = 1;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(104, 45);
            this.trackBar1.TabIndex = 6;
            this.trackBar1.Value = 5;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(-1, 397);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "5";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPicker
            // 
            this.colorPicker.BackColor = System.Drawing.Color.Black;
            this.colorPicker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.colorPicker.Location = new System.Drawing.Point(40, 431);
            this.colorPicker.Name = "colorPicker";
            this.colorPicker.Size = new System.Drawing.Size(40, 40);
            this.colorPicker.TabIndex = 5;
            this.colorPicker.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.colorPicker, "Wybierz kolor");
            this.colorPicker.Click += new System.EventHandler(this.colorPicker_Click);
            // 
            // canvas
            // 
            this.canvas.AutoScroll = true;
            this.canvas.AutoSize = true;
            this.canvas.BackColor = System.Drawing.Color.White;
            this.canvas.ContextMenuStrip = this.contextMenuStrip1;
            this.canvas.Location = new System.Drawing.Point(0, 0);
            this.canvas.Margin = new System.Windows.Forms.Padding(0);
            this.canvas.MinimumSize = new System.Drawing.Size(800, 600);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(800, 600);
            this.canvas.TabIndex = 0;
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.canvas.MouseClick += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseClick);
            this.canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kopiujToolStripMenuItem1,
            this.wklejToolStripMenuItem1,
            this.usuńToolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(151, 70);
            // 
            // kopiujToolStripMenuItem1
            // 
            this.kopiujToolStripMenuItem1.Name = "kopiujToolStripMenuItem1";
            this.kopiujToolStripMenuItem1.ShortcutKeyDisplayString = "Ctrl+C";
            this.kopiujToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.kopiujToolStripMenuItem1.Size = new System.Drawing.Size(150, 22);
            this.kopiujToolStripMenuItem1.Text = "Kopiuj";
            this.kopiujToolStripMenuItem1.Click += new System.EventHandler(this.kopiujToolStripMenuItem1_Click);
            // 
            // wklejToolStripMenuItem1
            // 
            this.wklejToolStripMenuItem1.Name = "wklejToolStripMenuItem1";
            this.wklejToolStripMenuItem1.ShortcutKeyDisplayString = "Ctrl+V";
            this.wklejToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.wklejToolStripMenuItem1.Size = new System.Drawing.Size(150, 22);
            this.wklejToolStripMenuItem1.Text = "Wklej";
            this.wklejToolStripMenuItem1.Click += new System.EventHandler(this.wklejToolStripMenuItem1_Click);
            // 
            // usuńToolStripMenuItem1
            // 
            this.usuńToolStripMenuItem1.Name = "usuńToolStripMenuItem1";
            this.usuńToolStripMenuItem1.ShortcutKeyDisplayString = "Delete";
            this.usuńToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.usuńToolStripMenuItem1.Size = new System.Drawing.Size(150, 22);
            this.usuńToolStripMenuItem1.Text = "Usuń";
            this.usuńToolStripMenuItem1.Click += new System.EventHandler(this.usuńToolStripMenuItem1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.edycjaToolStripMenuItem,
            this.obrazToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(984, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nowyToolStripMenuItem,
            this.toolStripSeparator1,
            this.zapiszToolStripMenuItem,
            this.zapiszJakoToolStripMenuItem,
            this.toolStripSeparator2,
            this.zakończToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // nowyToolStripMenuItem
            // 
            this.nowyToolStripMenuItem.Name = "nowyToolStripMenuItem";
            this.nowyToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.nowyToolStripMenuItem.Text = "Nowy";
            this.nowyToolStripMenuItem.Click += new System.EventHandler(this.nowyToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(144, 6);
            // 
            // zapiszToolStripMenuItem
            // 
            this.zapiszToolStripMenuItem.Name = "zapiszToolStripMenuItem";
            this.zapiszToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+S";
            this.zapiszToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.zapiszToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.zapiszToolStripMenuItem.Text = "Zapi&sz";
            this.zapiszToolStripMenuItem.Click += new System.EventHandler(this.zapiszToolStripMenuItem_Click);
            // 
            // zapiszJakoToolStripMenuItem
            // 
            this.zapiszJakoToolStripMenuItem.Name = "zapiszJakoToolStripMenuItem";
            this.zapiszJakoToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.zapiszJakoToolStripMenuItem.Text = "Zapisz jako...";
            this.zapiszJakoToolStripMenuItem.Click += new System.EventHandler(this.zapiszJakoToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(144, 6);
            // 
            // zakończToolStripMenuItem
            // 
            this.zakończToolStripMenuItem.Name = "zakończToolStripMenuItem";
            this.zakończToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.zakończToolStripMenuItem.Text = "Zakończ";
            // 
            // edycjaToolStripMenuItem
            // 
            this.edycjaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kopiujToolStripMenuItem,
            this.wklejToolStripMenuItem,
            this.usuńToolStripMenuItem});
            this.edycjaToolStripMenuItem.Name = "edycjaToolStripMenuItem";
            this.edycjaToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.edycjaToolStripMenuItem.Text = "Edycja";
            // 
            // kopiujToolStripMenuItem
            // 
            this.kopiujToolStripMenuItem.Name = "kopiujToolStripMenuItem";
            this.kopiujToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+C";
            this.kopiujToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.kopiujToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.kopiujToolStripMenuItem.Text = "Kopiuj";
            // 
            // wklejToolStripMenuItem
            // 
            this.wklejToolStripMenuItem.Name = "wklejToolStripMenuItem";
            this.wklejToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+V";
            this.wklejToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.wklejToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.wklejToolStripMenuItem.Text = "Wklej";
            // 
            // usuńToolStripMenuItem
            // 
            this.usuńToolStripMenuItem.Name = "usuńToolStripMenuItem";
            this.usuńToolStripMenuItem.ShortcutKeyDisplayString = "Delete";
            this.usuńToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.usuńToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.usuńToolStripMenuItem.Text = "Usuń";
            this.usuńToolStripMenuItem.Click += new System.EventHandler(this.usuńToolStripMenuItem1_Click);
            // 
            // obrazToolStripMenuItem
            // 
            this.obrazToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wymiaryToolStripMenuItem,
            this.tłoToolStripMenuItem});
            this.obrazToolStripMenuItem.Name = "obrazToolStripMenuItem";
            this.obrazToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.obrazToolStripMenuItem.Text = "Obraz";
            // 
            // wymiaryToolStripMenuItem
            // 
            this.wymiaryToolStripMenuItem.Name = "wymiaryToolStripMenuItem";
            this.wymiaryToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.wymiaryToolStripMenuItem.Text = "Wymiary";
            this.wymiaryToolStripMenuItem.Click += new System.EventHandler(this.wymiaryToolStripMenuItem_Click);
            // 
            // tłoToolStripMenuItem
            // 
            this.tłoToolStripMenuItem.Name = "tłoToolStripMenuItem";
            this.tłoToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.tłoToolStripMenuItem.Text = "Tło";
            this.tłoToolStripMenuItem.Click += new System.EventHandler(this.tłoToolStripMenuItem_Click);
            // 
            // penColorDialog
            // 
            this.penColorDialog.AnyColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 639);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(984, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel2.Text = "toolStripStatusLabel2";
            // 
            // tMagnifier
            // 
            this.tMagnifier.Image = global::ImageEdit.Properties.Resources.magnifier;
            this.tMagnifier.Location = new System.Drawing.Point(60, 178);
            this.tMagnifier.Margin = new System.Windows.Forms.Padding(0);
            this.tMagnifier.Name = "tMagnifier";
            this.tMagnifier.Size = new System.Drawing.Size(59, 58);
            this.tMagnifier.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.tMagnifier.TabIndex = 7;
            this.tMagnifier.TabStop = false;
            this.toolTip1.SetToolTip(this.tMagnifier, "Lupa");
            this.tMagnifier.Click += new System.EventHandler(this.tMagnifier_Click);
            // 
            // tBucket
            // 
            this.tBucket.Image = global::ImageEdit.Properties.Resources.bucket;
            this.tBucket.Location = new System.Drawing.Point(1, 178);
            this.tBucket.Margin = new System.Windows.Forms.Padding(0);
            this.tBucket.Name = "tBucket";
            this.tBucket.Size = new System.Drawing.Size(58, 58);
            this.tBucket.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.tBucket.TabIndex = 5;
            this.tBucket.TabStop = false;
            this.toolTip1.SetToolTip(this.tBucket, "Wypełnij kolorem");
            this.tBucket.Click += new System.EventHandler(this.tBucket_Click);
            // 
            // tPolygon
            // 
            this.tPolygon.Image = global::ImageEdit.Properties.Resources.polygon;
            this.tPolygon.Location = new System.Drawing.Point(60, 119);
            this.tPolygon.Margin = new System.Windows.Forms.Padding(0);
            this.tPolygon.Name = "tPolygon";
            this.tPolygon.Size = new System.Drawing.Size(59, 58);
            this.tPolygon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.tPolygon.TabIndex = 4;
            this.tPolygon.TabStop = false;
            this.toolTip1.SetToolTip(this.tPolygon, "Figura zamknięta");
            this.tPolygon.Click += new System.EventHandler(this.tPolygon_Click);
            // 
            // tTriangle
            // 
            this.tTriangle.Image = global::ImageEdit.Properties.Resources.triangle;
            this.tTriangle.Location = new System.Drawing.Point(1, 119);
            this.tTriangle.Margin = new System.Windows.Forms.Padding(0);
            this.tTriangle.Name = "tTriangle";
            this.tTriangle.Size = new System.Drawing.Size(58, 58);
            this.tTriangle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.tTriangle.TabIndex = 3;
            this.tTriangle.TabStop = false;
            this.toolTip1.SetToolTip(this.tTriangle, "Trójkąt");
            this.tTriangle.Click += new System.EventHandler(this.tTriangle_Click);
            // 
            // tCircle
            // 
            this.tCircle.Image = global::ImageEdit.Properties.Resources.circle;
            this.tCircle.Location = new System.Drawing.Point(60, 60);
            this.tCircle.Margin = new System.Windows.Forms.Padding(0);
            this.tCircle.Name = "tCircle";
            this.tCircle.Size = new System.Drawing.Size(59, 58);
            this.tCircle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.tCircle.TabIndex = 2;
            this.tCircle.TabStop = false;
            this.toolTip1.SetToolTip(this.tCircle, "Okrąg");
            this.tCircle.Click += new System.EventHandler(this.tCircle_Click);
            // 
            // tRectangle
            // 
            this.tRectangle.Image = global::ImageEdit.Properties.Resources.rectangle;
            this.tRectangle.Location = new System.Drawing.Point(1, 60);
            this.tRectangle.Margin = new System.Windows.Forms.Padding(0);
            this.tRectangle.Name = "tRectangle";
            this.tRectangle.Size = new System.Drawing.Size(58, 58);
            this.tRectangle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.tRectangle.TabIndex = 1;
            this.tRectangle.TabStop = false;
            this.toolTip1.SetToolTip(this.tRectangle, "Prostokąt");
            this.tRectangle.Click += new System.EventHandler(this.tRectangle_Click);
            // 
            // tLine
            // 
            this.tLine.Image = global::ImageEdit.Properties.Resources.line;
            this.tLine.Location = new System.Drawing.Point(60, 1);
            this.tLine.Margin = new System.Windows.Forms.Padding(0);
            this.tLine.Name = "tLine";
            this.tLine.Size = new System.Drawing.Size(59, 58);
            this.tLine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.tLine.TabIndex = 0;
            this.tLine.TabStop = false;
            this.toolTip1.SetToolTip(this.tLine, "Linia");
            this.tLine.Click += new System.EventHandler(this.tLine_Click);
            // 
            // tPointer
            // 
            this.tPointer.Image = global::ImageEdit.Properties.Resources.pointer;
            this.tPointer.Location = new System.Drawing.Point(1, 1);
            this.tPointer.Margin = new System.Windows.Forms.Padding(0);
            this.tPointer.Name = "tPointer";
            this.tPointer.Size = new System.Drawing.Size(58, 58);
            this.tPointer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.tPointer.TabIndex = 8;
            this.tPointer.TabStop = false;
            this.toolTip1.SetToolTip(this.tPointer, "Wskaźnik");
            this.tPointer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tPointer_MouseClick);
            // 
            // tSelection
            // 
            this.tSelection.Image = global::ImageEdit.Properties.Resources.selection;
            this.tSelection.Location = new System.Drawing.Point(1, 237);
            this.tSelection.Margin = new System.Windows.Forms.Padding(0);
            this.tSelection.Name = "tSelection";
            this.tSelection.Size = new System.Drawing.Size(58, 58);
            this.tSelection.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.tSelection.TabIndex = 6;
            this.tSelection.TabStop = false;
            this.toolTip1.SetToolTip(this.tSelection, "Zaznaczenie");
            this.tSelection.Click += new System.EventHandler(this.tSelection_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 661);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "Form1";
            this.Text = "ImageEdit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tMagnifier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tBucket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tPolygon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTriangle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tCircle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tRectangle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tPointer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tSelection)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label lineThickness;
        private BufferedPanel canvas;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edycjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem obrazToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nowyToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem zapiszToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zapiszJakoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem zakończToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kopiujToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wklejToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wymiaryToolStripMenuItem;
        private System.Windows.Forms.ColorDialog penColorDialog;
        private System.Windows.Forms.Label colorPicker;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox tLine;
        private System.Windows.Forms.PictureBox tBucket;
        private System.Windows.Forms.PictureBox tPolygon;
        private System.Windows.Forms.PictureBox tTriangle;
        private System.Windows.Forms.PictureBox tCircle;
        private System.Windows.Forms.PictureBox tRectangle;
        private System.Windows.Forms.Label tools;
        private System.Windows.Forms.ToolStripMenuItem tłoToolStripMenuItem;
        private System.Windows.Forms.ColorDialog bgColorDialog;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.PictureBox tSelection;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.PictureBox tMagnifier;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem kopiujToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem wklejToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem usuńToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem usuńToolStripMenuItem;
        private System.Windows.Forms.PictureBox tPointer;
    }
}

